from multiprocessing import Process
import subprocess
import time

T1 = time.perf_counter()


def check_host(ip):
    status, result = subprocess.getstatusoutput("ping -n 2 " + ip)
    if (status == 0):
        print(f'Host {ip} is UP')
    else:
        print(f'Host {ip} is DOWN')


if __name__ == '__main__':
    hosts = ["192.168.1.1", "192.168.1.2", "192.168.1.3", "8.8.8.8", "8.8.4.4"]
    Processes = []
    for x in hosts:
        ip = [x]
        P = Process(target=check_host, args=ip)
        P.start()
        Processes.append(P)
    for process in Processes:
        process.join()


    T2 = time.perf_counter()
    print(f"Selesai dalam {round(T2 - T1, 2)} detik")
