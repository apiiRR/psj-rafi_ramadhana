import time
from multiprocessing import Process

T1 = time.perf_counter()


def do_something():
    print("Diam sejenak .. 1 detik")
    time.sleep(1)
    print("Selesai berdiam...")


if __name__ == '__main__':
    P1 = Process(target=do_something)
    P2 = Process(target=do_something)
    P1.start()
    P2.start()
    P1.join()
    P2.join()
    T2 = time.perf_counter()
    print(f"Selesai dalam {round(T2-T1, 2)} detik")
# Processes = []
# for x in range(10):
#     P = multiprocessing.Process(target=do_something)
#     P.start()
#     Processes.append(P)
#
# for process in Processes:
#     process.join()
#
# T2 = time.perf_counter()
# print(f"Selesai dalam … {round(T2-T1,2)} detik")
