import threading
import subprocess
import time

T1 = time.perf_counter()


def check_host(ip):
    status, result = subprocess.getstatusoutput("ping -n 2 " + ip)
    if (status == 0):
        print(f'Host {ip} is UP')
    else:
        print(f'Host {ip} is DOWN')


hosts = ["192.168.1.1", "192.168.1.2", "192.168.1.3", "8.8.8.8", "8.8.4.4"]
Threads = []

for x in hosts:
    ip = [x]
    T = threading.Thread(target=check_host, args=ip)
    T.start()
    Threads.append(T)

for t in Threads:
    t.join()

T2 = time.perf_counter()
print(f"Selesai dalam {round(T2 - T1, 2)} detik")
