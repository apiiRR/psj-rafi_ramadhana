import concurrent.futures
import time

T1 = time.perf_counter()
def do_something(sec):
    print(f"Diam sejenak.... {sec} detik")
    time.sleep(sec)
    return "Selesai berdiam..."

with concurrent.futures.ThreadPoolExecutor() as executor:
    f = executor.submit(do_something, 2)
    print(f.result())

T2 = time.perf_counter()
print(f"Selesai dalam ... {round(T2-T1)} detik")