import time
import threading

T1 = time.perf_counter()


def do_something():
    print("Diam sejenak... 1 detik")
    time.sleep(1)
    print("Selesai berdiam...")


P1 = threading.Thread(target=do_something)
P2 = threading.Thread(target=do_something)
P1.start()
P2.start()
P1.join()
P2.join()
T2 = time.perf_counter()
print(f"Selesai dalam... {round(T2-T1)} detik")
