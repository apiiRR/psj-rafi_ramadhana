import requests
from bs4 import BeautifulSoup

web = requests.get("https://www.bmkg.go.id/gempabumi/gempabumi-terkini.bmkg")
data = web.text

bs = BeautifulSoup(data, "html.parser")

rows = []

data_rows = bs.find_all('tr')
for row in data_rows:
    value = row.find_all('td')
    beautified_value = [ele.text.strip() for ele in value]
    rows.append(beautified_value)

for row in rows[1:]:
    # print (row)
    print (
    "No: %s\nWaktu Gempa: %s \nLintang: %s \nBujur: %s \nMagnitudo: %s \nKedalaman: %s \nWilayah: %s\n" % \
    (row[0], row[1], row[2], row[3], row[4], row[5], row[6]))








